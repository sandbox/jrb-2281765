(function($){ // Create local scope.

  var hash = window.location.hash;
  var primaryHost = Drupal.settings.single_db_sso.primary_host;
  var statusCheckURL = Drupal.settings.single_db_sso.status_check_url;

  var check_sso_status = function() {
    var currentSite = window.location.protocol + '//' + window.location.host + '/';
    $.ajax({
      dataType: 'jsonp',
      url: statusCheckURL,
      type: 'GET',
      data: {site : currentSite},
      success: function(data){
        process_sso_check(data);
      }
    });
  };

  var process_sso_check = function(data) {
    if (data !== '0') {
      window.location = '/single-db-sso/login/' + data + '/complete?destination=' + escape(window.location);
    }
  };

  if (window.location.host != primaryHost && hash != '#no-sso') {
    check_sso_status();
  }

})(jQuery);

