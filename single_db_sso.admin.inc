<?php
function single_db_sso_admin_settings_form($form, &$form_state) {

  $form['single_db_sso_method'] = array(
    '#type' => 'radios',
    '#title' => t('SSO method to use'),
    '#description' => t('<ul><li>"On Demand" means that the secondary sites will use '
      . 'JavaScript to check if the user is logged in to the primary site. If '
      . 'they are, they will be redirected to an auto-login page and then return.</li><li>'
      . '"Auto" means that the user will be immediately logged in to all sites '
      . 'specified below when the user logs in to any site. The "Auto" method '
      . ' may put a heavy load on the server if there are many sites specified.</li></ul>'),
    '#options' => array(
      SINGLE_DB_SSO_METHOD_ON_DEMAND => t('On Demand'),
      SINGLE_DB_SSO_METHOD_AUTO => t('Auto'),
    ),
    '#default_value' => variable_get('single_db_sso_method', SINGLE_DB_SSO_METHOD_ON_DEMAND),
    '#required' => TRUE,
  );

  $form['single_db_sso_primary_site'] = array(
    '#type' => 'textfield',
    '#title' => t('Primary site'),
    '#description' => t('If using "On Demand" method, all other sites will '
      .'check this site to determine if the user should be logged in. Include the http://'),
    '#default_value' => variable_get('single_db_sso_primary_site', NULL),
    '#attributes' => array(
      'placeholder' => $GLOBALS['base_url'],
    ),
  );

  $form['single_db_sso_site_list'] = array(
    '#type' => 'textarea',
    '#title' => t('SSO sites'),
    '#description' => t('If using "Auto" method, user will be automatically '
      . 'logged in to all these sites if they log in to any site. One site per '
      . 'line, include the http://'),
    '#default_value' => variable_get('single_db_sso_site_list', NULL),
    '#attributes' => array(
      'placeholder' => 'http://example.com',
    ),
  );

  $form['single_db_sso_region'] = array(
    '#type' => 'textfield',
    '#title' => t('Theme region to contain SSO markup'),
    '#description' => t('Theme region to add login markup to. Must be valid for theme.'),
    '#default_value' => variable_get('single_db_sso_region', SINGLE_DB_SSO_REGION),
    '#attributes' => array(
      'placeholder' => SINGLE_DB_SSO_REGION,
    ),
    '#required' => TRUE,
  );

  global $session_name;
  if (!empty($session_name)) {
    $form['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['advanced']['single_db_sso_use_single_session'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use single session'),
      '#description' => t('If checked, the user will use the same session ID across all sites. This requires a patch to Drupal core.'),
      '#default_value' => variable_get('single_db_sso_use_single_session', 0),
    );
  } else {
    $form['single_db_sso_use_single_session'] = array(
      '#type' => 'hidden',
      '#value' => 0,
    );
  }

  return system_settings_form($form);

}

function single_db_sso_admin_settings_form_validate($form, &$form_state) {

  $method = $form_state['values']['single_db_sso_method'];

  switch ($method) {
    case SINGLE_DB_SSO_METHOD_ON_DEMAND:
      if (empty($form_state['values']['single_db_sso_primary_site'])) {
        form_set_error('single_db_sso_primary_site', t('Please enter a Primary site when using "On Demand" SSO.'));
      }
      break;
    case SINGLE_DB_SSO_METHOD_AUTO:
      if (empty($form_state['values']['single_db_sso_site_list'])) {
        form_set_error('single_db_sso_site_list', t('Please enter some SSO sites when using "Auto" SSO.'));
      }
      break;
  }

}
