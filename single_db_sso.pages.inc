<?php
function single_db_sso_complete_login($token) {

  global $user;

  drupal_page_is_cacheable(FALSE);

  $info = _single_db_sso_get_info_from_token($token, 'login');

  // @todo - Handle failed db call.

  if ($info && empty($user->uid)) {
    // @todo - What if they're already logged in as someone else?
    $users = user_load_multiple(array($info->uid), array('status' => '1'));
    $account = reset($users);
    if (!empty($account->uid)) {
      // Set the new user.
      $user = $account;
      $user->sid = $info->sid;
      $user->grouping = $info->grouping;
      _single_db_sso_log_user_in($user, $token);
    }
  }

  $destination = drupal_get_destination();
  if (!empty($destination['destination'])) {
    $destination = $destination['destination'];
  } else {
    $destination = '<front>';
  }

  if (empty($user->uid)) {
    // Handle failed login so we don't keep redirecting.
    $parts = explode('#', $destination);
    $destination = $parts[0] . '#no-sso';
  }

  drupal_goto($destination);

}

function single_db_sso_process_login($token) {

  global $user;

  drupal_page_is_cacheable(FALSE);

  if (!$info = _single_db_sso_get_info_from_token($token, 'login')) {
    // Invalid token.
    _single_db_sso_serve_image();
    return;
  }

  if (!empty($user->uid) && $user->uid == $info->uid) {
    // Already logged in.
    // @todo - What if they're already logged in as someone else?
    _single_db_sso_serve_image();
    return;
  }

  $users = user_load_multiple(array($info->uid), array('status' => '1'));
  $account = reset($users);
  if (!empty($account->uid)) {
    // Set the new user.
    $user = $account;
    $user->sid = $info->sid;
    $user->grouping = $info->grouping;
    _single_db_sso_log_user_in($user, $token);
  }

  _single_db_sso_serve_image();

}

function single_db_sso_check_status() {

  global $user;

  $token = '0';
  $site = (!empty($_GET['site'])) ? $_GET['site'] : NULL;

  if ($site && !empty($user->uid)) {
    $grouping = (!empty($_SESSION['single_db_sso_grouping'])) ? $_SESSION['single_db_sso_grouping'] : NULL;
    $token = _single_db_sso_create_token('login', $user->uid, $site, session_id(), $grouping);
    if (!$grouping) $_SESSION['single_db_sso_grouping'] = $token;
  }

  $callback = (!empty($_REQUEST['callback'])) ? check_plain($_REQUEST['callback']) : NULL;

  // If its a jsonp callback request
  if ($callback) {
    $json_response = drupal_json_encode($token);
    header("Content-type: text/javascript");
    echo $callback ."(". $json_response .");";
  }
  else {
    drupal_json_output($token);
  }

}
